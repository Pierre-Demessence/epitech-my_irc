/*
** list.h for my_irc in /home/demess_p/rendu/PSU_2013_myirc
**
** Made by demess_p
** Login   <demess_p@epitech.net>
**
** Started on  Mon Apr 21 17:05:52 2014 demess_p
** Last update Fri Apr 25 18:32:31 2014 demess_p
*/

#ifndef		LIST_H_
# define	LIST_H_

typedef struct	s_node
{
  void		*data;
  struct s_node	*next;
  struct s_node	*prev;
}		t_node;

typedef struct	s_list
{
  t_node	*head;
  t_node	*tail;
  unsigned int	length;
}		t_list;

t_list		*list_new();
void		list_delete(t_list *list, char freedata);
int		list_push(t_list *list, void *data);
int		list_pop(t_list *list, int n, char freedata);
t_node		*list_find(t_list *list, unsigned int n);

#endif /* !LIST_H_ */
