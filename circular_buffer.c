/*
** circular_buffer.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Thu Apr 24 11:55:50 2014 taieb_t$
** Last update Thu Apr 24 14:16:15 2014 taieb_t$
*/

#include <stdlib.h>
#include <string.h>
#include "circular_buffer.h"

void		init_circular_buffer(t_circular_buffer *cb)
{
  memset(cb->buffer, 0, NB_BUFFER * BUFFER_SIZE);
  cb->start = 0;
  cb->count = 0;
}

void		write_next(t_circular_buffer *cb, char *buffer)
{
  int		pos;

  pos = (cb->start + cb->count) % NB_BUFFER;
  memset(cb->buffer[pos], 0, BUFFER_SIZE);
  strcpy(cb->buffer[pos], buffer);
  if (cb->count == BUFFER_SIZE)
    cb->start = (cb->start + 1) % NB_BUFFER;
  else
    ++(cb->count);
}

char		*read_next(t_circular_buffer *cb)
{
  char		*res;

  res = cb->buffer[cb->start];
  cb->start = (cb->start + 1) % NB_BUFFER;
  --cb->count;
  return (res);
}
