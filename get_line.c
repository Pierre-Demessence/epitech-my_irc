/*
** get_line.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Mon Apr 21 19:04:24 2014 taieb_t$
** Last update Sun Apr 27 11:30:36 2014 taieb_t$
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "client.h"

char			get_cmd(t_client *client)
{
  int			res;

  memset(client->buffer, 0, BUFFER_SIZE);
  if ((res = read(0, client->buffer, BUFFER_SIZE - 1)) <= 0)
    {
      fprintf(stderr, "read failed\n");
      return (1);
    }
  client->write = 1;
  return (0);
}
