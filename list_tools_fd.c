/*
** list_tools_fd.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Tue Apr 22 15:55:00 2014 taieb_t$
** Last update Sun Apr 27 11:38:29 2014 taieb_t$
*/

#include "server.h"

void			set_all_fd(t_server *server)
{
  unsigned int		i;
  t_client_infos	*client;

  FD_ZERO(&server->readfs);
  FD_ZERO(&server->writefs);
  FD_SET(server->sfd, &server->readfs);
  i = 0;
  while (i < server->list->length)
    {
      client = ((t_client_infos *)list_find(server->list, i)->data);
      FD_SET(client->cs, &server->readfs);
      FD_SET(client->cs, &server->writefs);
      ++i;
    }
}

int		get_max(t_server *server)
{
  unsigned int		i;
  int			res;
  int			cs;

  i = 0;
  res = server->sfd;
  while (i < server->list->length)
    {
      cs = ((t_client_infos *)list_find(server->list, i)->data)->cs;
      if (cs > res)
  	res = cs;
      ++i;
    }
  return (res + 1);
}
