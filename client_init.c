/*
** client_init.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Mon Apr 21 18:30:47 2014 taieb_t$
** Last update Sun Apr 27 17:20:40 2014 demess_p
*/

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "client.h"

static void	client_connect(t_client *client)
{
  client->sin.sin_family = AF_INET;
  client->sin.sin_port = htons(client->port);
  client->sin.sin_addr.s_addr = inet_addr(client->ip_addr);
  if (connect(client->sfd, (const struct sockaddr *)&(client->sin),
	      sizeof(client->sin)) == -1)
    {
      perror("connect failed ");
      close(client->sfd);
    }
  client->buffer[0] = '\0';
}

void		client_init(char *line, t_client *client)
{
  char		*port;

  if ((port = strchr(line, ':')) != NULL)
    {
      port[0] = 0;
      client->ip_addr = line + strlen("/server ");
      client->port = atoi(port + 1);
    }
  else
    client->ip_addr = line + strlen("/server ");
  client_connect(client);
}
