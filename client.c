/*
** client.c|epitech-my_irc for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Mon Apr 21 13:49:30 2014 taieb_t$
** Last update Sun Apr 27 11:28:38 2014 taieb_t$
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include "client.h"
#include "port.h"

static char		init_client_struct(t_client *client)
{
  if ((client->pe = getprotobyname("TCP")) == NULL)
    {
      printf("getprotobyname fail\n");
      return (1);
    }
  if ((client->sfd = socket(AF_INET, SOCK_STREAM, client->pe->p_proto)) == -1)
    {
      fprintf(stderr, "error socket\n");
      return (1);
    }
  client->pe = NULL;
  client->port = DEFAULT_PORT;
  client->ip_addr = 0;
  client->write = 0;
  return (0);
}

static void		init_all_fd(t_client *client)
{
  FD_ZERO(&client->readfs);
  FD_ZERO(&client->writefs);
  FD_SET(0, &client->readfs);
  FD_SET(client->sfd, &client->readfs);
  if (client->write)
    FD_SET(client->sfd, &client->writefs);
}

static char		is_something_here(t_client *client)
{
  char			res;

  if (FD_ISSET(0, &client->readfs))
    {
      res = get_cmd(client);
      if (res)
	return (res);
      else if (strncmp(client->buffer, "/server ", strlen("/server ")) == 0)
	client_init(client->buffer, client);
      else if (strncmp(client->buffer, "/quit", strlen("/quit")) == 0)
	{
	  close(client->sfd);
	  init_client_struct(client);
	}
      return (res);
    }
  else if (FD_ISSET(client->sfd, &client->readfs))
    return (sock_read(client));
  else if (FD_ISSET(client->sfd, &client->writefs))
    return (sock_write(client));
  return (0);
}

static void		main_loop(t_client *client)
{
  while (1)
    {
      init_all_fd(client);
      if (select(client->sfd + 1, &client->readfs, &client->writefs,
		 NULL, NULL) == -1)
	break ;
      if (is_something_here(client))
	break ;
    }
}

int			main()
{
  t_client		client;

  if (init_client_struct(&client))
    return (1);
  sigquit(client.sfd);
  (void)signal(SIGINT, sigquit);
  main_loop(&client);
  return (0);
}
