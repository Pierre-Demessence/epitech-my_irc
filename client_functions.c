/*
** client_functions.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Thu Apr 24 20:30:49 2014 taieb_t$
** Last update Sun Apr 27 11:30:16 2014 taieb_t$
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "client.h"

char		sock_write(t_client *client)
{
  write(client->sfd, client->buffer, strlen(client->buffer));
  memset(client->buffer, 0, BUFFER_SIZE);
  client->write = 0;
  return (0);
}

char		sock_read(t_client *client)
{
  int		res;

  if ((res = read(client->sfd, client->buffer, BUFFER_SIZE - 1)) <= 0)
    return (0);
  write(1, client->buffer, res);
  memset(client->buffer, 0, BUFFER_SIZE);
  return (0);
}

void		sigquit(int fd)
{
  static char	my_bool = 0;
  static int	fd_tempo = 0;

  if (my_bool)
    {
      close(fd_tempo);
    }
  else
    {
      fd_tempo = fd;
      my_bool = 1;
    }
}
