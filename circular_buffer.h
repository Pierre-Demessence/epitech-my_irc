/*
** circular_buffer.h for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Thu Apr 24 11:55:42 2014 taieb_t$
** Last update Thu Apr 24 14:31:03 2014 taieb_t$
*/

#ifndef		CIRCULAR_BUFFER_H_
# define	CIRCULAR_BUFFER_H_

# define	NB_BUFFER 8
# define	BUFFER_SIZE 512

typedef struct	s_circular_buffer
{
  char		buffer[NB_BUFFER][BUFFER_SIZE];
  unsigned int	start;
  unsigned int	count;
}		t_circular_buffer;

void		init_circular_buffer(t_circular_buffer *);
char		is_empty(t_circular_buffer *);
char		*read_next(t_circular_buffer *);
void		write_next(t_circular_buffer *, char *);

#endif /* !CIRCULAR_BUFFER_H_ */
