/*
** server_add_client.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Tue Apr 22 12:21:19 2014 taieb_t$
** Last update Sun Apr 27 17:53:23 2014 demess_p
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "channels.h"
#include "server.h"

void		add_client(t_server *server)
{
  t_client_infos	*new_client;

  new_client = malloc(sizeof(t_client_infos));
  if (new_client == NULL)
    return ;
  if ((new_client->c_buffer = malloc(sizeof (t_circular_buffer))) == NULL)
    return ;
  if ((new_client->c_buffer_w = malloc(sizeof (t_circular_buffer))) == NULL)
    return ;
  new_client->len = 0;
  init_circular_buffer(new_client->c_buffer);
  init_circular_buffer(new_client->c_buffer_w);
  new_client->cs = accept(server->sfd,
			  (struct sockaddr *)&(new_client->sin_client),
			  (socklen_t *)&(new_client->len));
  memset(new_client->nickname, 0, NICKNAME_SIZE);
  memset(new_client->channel, 0, CHANNEL_SIZE);
  sprintf(new_client->channel, "%s", DEFAULT_CHANNEL);
  sprintf(new_client->nickname, "%s%d", DEFAULT_NICKNAME, new_client->cs);
  server_message_send(server, new_client->nickname, "Home");
  server_message_send(server, " : join the channel\n", "Home");
  if (list_push(server->list, new_client))
    free (new_client);
  printf("new client : [%d]\n", new_client->cs);
}
