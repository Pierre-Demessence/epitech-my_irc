/*
** client.h|epitech-my_irc for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Mon Apr 21 15:01:06 2014 taieb_t$
** Last update Wed Apr 30 14:59:26 2014 demess_p
*/

#ifndef			CLIENT_H_
# define		CLIENT_H_

# include		<sys/select.h>
# include		<sys/types.h>
# include		<netdb.h>
# include		"circular_buffer.h"

typedef struct		s_client
{
  fd_set		readfs;
  fd_set		writefs;
  char			write;
  char			buffer[BUFFER_SIZE];
  int			sfd;
  struct sockaddr_in	sin;
  struct protoent	*pe;
  uint16_t		port;
  char			*ip_addr;
}			t_client;

/*
** client_init.c
*/
void		client_init(char *, t_client *);

/*
** get_line.c
*/
char		get_cmd(t_client *);

/*
** client_functions.c
*/
char		sock_write(t_client *);
char		sock_read(t_client *);
void		sigquit(int);

#endif /* !CLIENT_H_ */
