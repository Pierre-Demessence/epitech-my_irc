/*
** server.h|epitech-my_irc for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Mon Apr 21 16:15:54 2014 taieb_t$
** Last update Sun Apr 27 18:05:12 2014 demess_p
*/

#ifndef			SERVER_H_
# define		SERVER_H_

# include		<sys/types.h>
# include		<netdb.h>
# include		<sys/select.h>
# include		"list.h"
# include		"circular_buffer.h"

# define		NICKNAME_SIZE 256
# define		DEFAULT_NICKNAME "Anonymous"

# define		CHANNEL_SIZE 256
# define		DEFAULT_CHANNEL "Home"

typedef struct		s_client_infos
{
  int			cs;
  int			len;
  char			*last_read;
  t_circular_buffer	*c_buffer;
  t_circular_buffer	*c_buffer_w;
  char			buffer_tempo[BUFFER_SIZE];
  char			nickname[NICKNAME_SIZE];
  char			channel[CHANNEL_SIZE];
  struct sockaddr_in	sin_client;
}			t_client_infos;

typedef struct		s_server
{
  int			sfd;
  struct sockaddr_in	sin;
  uint16_t		port;
  fd_set		readfs;
  fd_set		writefs;
  t_list		*list;
}			t_server;

/*
** server_init.c
*/
char			init_server(t_server *);

/*
** server_signal.c
*/
void			sigquit(unsigned long);

/*
** server_add_client.c
*/
void			add_client(t_server *);

/*
** list_tools_fd.c
*/
void			set_all_fd(t_server *);
int			get_max(t_server *);

/*
** server_recieve.c
*/
void			server_get_line(t_server *, t_client_infos *);

/*
** server_functions.c
*/
void			server_nickname(t_server *, t_client_infos *);
void			server_private_message(t_server *, t_client_infos *);
void			server_message_send(t_server *server,
					    char *msg,
					    char *channel);
void			basic_message_send(t_server *, t_client_infos *);

/*
** server_delete_client.c
*/
void			delete_client(t_server *, t_client_infos *);

#endif /* !SERVER_H_ */
