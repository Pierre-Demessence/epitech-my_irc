/*
** server_init.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Mon Apr 21 17:29:16 2014 taieb_t$
** Last update Tue Apr 22 12:20:48 2014 taieb_t$
*/

#include "server_init.h"
#include "server.h"

char			init_server(t_server *infos)
{
  struct protoent	*pe;

  pe = getprotobyname("TCP");
  if ((infos->sfd = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    {
      fprintf(stderr, "error socket\n");
      return (1);
    }
  infos->sin.sin_family = AF_INET;
  infos->sin.sin_port = htons(infos->port);
  infos->sin.sin_addr.s_addr = INADDR_ANY;
  if (bind(infos->sfd, (const struct sockaddr *)&(infos->sin),
	   sizeof(infos->sin)) == -1)
    {
      fprintf(stderr, "error bind\n");
      close(infos->sfd);
      return (1);
    }
  if (listen(infos->sfd, 10) == -1)
    {
      fprintf(stderr, "error listen\n");
      close(infos->sfd);
      return (1);
    }
  return (0);
}
