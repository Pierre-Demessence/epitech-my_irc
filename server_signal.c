/*
** server_signal.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Tue Apr 22 12:18:12 2014 taieb_t$
** Last update Tue Apr 22 12:19:24 2014 taieb_t$
*/

#include <stdlib.h>
#include <unistd.h>
#include "server.h"

void			sigquit(unsigned long sig)
{
  static t_server	*ptr = NULL;

  if (ptr == NULL)
    ptr = (t_server *)sig;
  else
    {
      list_delete(ptr->list, 1);
      (void)close(ptr->sfd);
    }
}
