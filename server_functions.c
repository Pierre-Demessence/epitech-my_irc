/*
** server_nickname.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Tue Apr 22 16:01:12 2014 taieb_t$
** Last update Sun Apr 27 18:18:54 2014 demess_p
*/

#include <unistd.h>
#include <string.h>
#include "server.h"

static void		change_nickname(char *old, t_client_infos *client)
{
  memset(client->buffer_tempo, 0, BUFFER_SIZE);
  strcat(client->buffer_tempo, old);
  strcat(client->buffer_tempo, " change nickname to : ");
  memset(client->nickname, 0, NICKNAME_SIZE);
  strcpy(client->nickname, &client->last_read[6]);
  strcat(client->buffer_tempo, client->nickname);
  strcat(client->buffer_tempo, "\n");
  return ;
}

void			server_nickname(t_server *server,
					t_client_infos *client)
{
  unsigned int		i;
  t_client_infos	*dest;
  char			*msg;
  char			old[NICKNAME_SIZE];

  if ((msg = strchr(client->last_read, '\n')) == NULL)
    return ;
  msg[0] = 0;
  i = 0;
  strcpy(old, client->nickname);
  while (i < server->list->length)
    {
      change_nickname(old, client);
      dest = (t_client_infos *)(list_find(server->list, i)->data);
      if (client != dest)
	memcpy(dest->buffer_tempo, client->buffer_tempo,
	       strlen(client->buffer_tempo));
      write_next(dest->c_buffer_w, dest->buffer_tempo);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      ++i;
    }
}

void			server_private_message(t_server *server,
					       t_client_infos *client)
{
  unsigned int		i;
  t_client_infos	*dest;
  char			*msg;
  char			*cmp_nickname;

  i = 0;
  cmp_nickname = &client->last_read[5];
  if ((msg = strchr(cmp_nickname, ' ')) == NULL)
    return ;
  msg[0] = 0;
  while (i < server->list->length)
    {
      dest = (t_client_infos *)(list_find(server->list, i)->data);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      if (strcmp(dest->nickname, cmp_nickname) == 0)
  	{
	  strcat(dest->buffer_tempo, client->nickname);
	  strcat(dest->buffer_tempo, ": ");
	  strcat(dest->buffer_tempo, msg + 1);
  	}
      write_next(dest->c_buffer_w, dest->buffer_tempo);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      ++i;
    }
}

void			server_message_send(t_server *server,
					    char *msg,
					    char *channel)
{
  unsigned int		i;
  t_client_infos	*dest;

  i = 0;
  while (i < server->list->length)
    {
      dest = (t_client_infos *)(list_find(server->list, i)->data);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      if (strcmp(dest->channel, channel) == 0
	  && strlen(dest->channel) > 0)
	strcat(dest->buffer_tempo, msg);
      write_next(dest->c_buffer_w, dest->buffer_tempo);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      ++i;
    }
}

void			basic_message_send(t_server *server,
					   t_client_infos *client)
{
  char			buf[BUFFER_SIZE];

  memset(buf, 0, BUFFER_SIZE);
  strcat(buf, client->nickname);
  strcat(buf, " : ");
  strcat(buf, client->last_read);
  server_message_send(server, buf, client->channel);
}
