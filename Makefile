##
## Makefile for eval_expr in /home/taieb_t/rendu/Piscine-C-eval_expr
## 
## Made by taieb_t$
## Login   <taieb_t@epitech.net>
## 
## Started on  Mon Oct 21 20:33:30 2013 taieb_t$
## Last update Sun Apr 27 18:05:21 2014 demess_p
##

CC	=	gcc

RM	=	rm -f

SRC	=	server.c		\
\
		list.c			\
		circular_buffer.c	\
\
		server_init.c		\
		server_signal.c		\
		server_add_client.c	\
		list_tools_fd.c		\
		server_functions.c	\
		server_recieve.c	\
		channels_functions.c	\
		server_delete_client.c

SRC2	=	client.c		\
		client_init.c		\
		get_line.c		\
		client_functions.c	\
		circular_buffer.c

OBJS	=	$(SRC:.c=.o)

OBJS2	=	$(SRC2:.c=.o)

NAME	=	serveur

CLIENT	=	client

CFLAGS	+=	-Wextra -Wall -Werror
CFLAGS	+=	-ansi -pedantic
#CFLAGS	+=	-ggdb3 -O0

all: $(NAME) $(CLIENT)

$(CLIENT): $(OBJS2)
	$(CC) -o $(CLIENT) $(OBJS2)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(OBJS)

clean:
	$(RM) $(OBJS)
	$(RM) $(OBJS2)

fclean: clean
	$(RM) $(NAME)
	$(RM) $(CLIENT)

re: fclean all

.PHONY: all clean fclean re
