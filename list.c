/*
** list.c for my_irc in /home/demess_p/rendu/PSU_2013_myirc
**
** Made by demess_p
** Login   <demess_p@epitech.net>
**
** Started on  Mon Apr 21 17:05:48 2014 demess_p
** Last update Sat Apr 26 17:52:08 2014 demess_p
*/

#include	<stdlib.h>
#include	"list.h"

t_list		*list_new()
{
  t_list	*res;
  res = malloc(sizeof(t_list));
  if (res)
    {
      res->head = NULL;
      res->tail = NULL;
      res->length = 0;
    }
  return (res);
}

void		list_delete(t_list *list, char freedata)
{
  while (list->length)
    list_pop(list, -1, freedata);
  free(list);
}

int		list_push(t_list *list, void *data)
{
  t_node	*node;

  node = malloc(sizeof(t_node));
  if (!node)
    return (-1);
  node->next = NULL;
  node->prev = list->tail;
  node->data = data;
  if (list->tail == NULL)
    {
      list->head = node;
      list->tail = node;
    }
  else
    {
      list->tail->next = node;
      list->tail = node;
    }
  ++list->length;
  return (0);
}

int		list_pop(t_list *list, int n, char freedata)
{
  t_node	*todel;

  if (n < -1 || n >= (int) list->length)
    return (-1);
  if (n == -1)
    n = list->length - 1;
  todel = list_find(list, n);
  if (todel != list->head)
    todel->prev->next = todel->next;
  if (todel != list->tail)
  todel->next->prev = todel->prev;
  if (todel == list->head)
    list->head = todel->next;
  if (todel == list->tail)
      list->tail = todel->prev;
  if (freedata)
    free(todel->data);
  free(todel);
  --list->length;
  return (0);
}

t_node		*list_find(t_list *list, unsigned int n)
{
  unsigned int	i;
  t_node	*tmp;

  if (list->length == 0 || n >= list->length)
    return (NULL);
  tmp = list->head;
  i = 0;
  while (i < n)
    {
      tmp = tmp->next;
      ++i;
    }
  return (tmp);
}
