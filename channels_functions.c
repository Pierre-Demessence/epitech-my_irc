/*
** channels_functions.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Tue Apr 22 18:26:19 2014 taieb_t$
** Last update Sun Apr 27 11:39:00 2014 taieb_t$
*/

#include "channels.h"

static void		send_rejoin(t_server *server,
				    t_client_infos *client,
				    char *channel,
				    char join)
{
  unsigned int		i;
  t_client_infos	*dest;

  i = 0;
  while (i < server->list->length)
    {
      dest = (t_client_infos *)(list_find(server->list, i)->data);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      if (strcmp(dest->channel, channel) == 0)
  	{
	  strcat(dest->buffer_tempo, client->nickname);
	  if (join)
	    strcat(dest->buffer_tempo, " : join the channel ");
	  else
	    strcat(dest->buffer_tempo, " : leave the channel ");
	  strcat(dest->buffer_tempo, channel);
	  strcat(dest->buffer_tempo, "\n");
	}
      write_next(dest->c_buffer_w, dest->buffer_tempo);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      ++i;
    }
}

void			join_channel(t_server *server,
				     t_client_infos *client)
{
  char			*channel;
  char			*eof;

  channel = &client->last_read[6];
  if ((eof = strchr(channel, '\n')) == NULL)
    return ;
  eof[0] = 0;
  memset(client->channel, 0, CHANNEL_SIZE);
  strcpy(client->channel, channel);
  send_rejoin(server, client, channel, 1);
}

void			leave_channel(t_server *server,
				      t_client_infos *client)
{
  char			*channel;
  char			*eof;

  channel = &client->last_read[6];
  if ((eof = strchr(channel, '\n')) == NULL)
    return ;
  eof[0] = 0;
  send_rejoin(server, client, channel, 0);
  memset(client->channel, 0, CHANNEL_SIZE);
}

static void		send_user_list(t_server *server,
				       t_client_infos *client,
				       char *list)
{
  unsigned int		i;
  t_client_infos	*dest;

  i = 0;
  while (i < server->list->length)
    {
      dest = (t_client_infos *)(list_find(server->list, i)->data);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      if (dest == client)
	{
	  strcat(dest->buffer_tempo, "user list :\n");
	  strcat(dest->buffer_tempo, list);
	  strcat(dest->buffer_tempo, "\n");
	}
      write_next(dest->c_buffer_w, dest->buffer_tempo);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      ++i;
    }
}

void			users_list(t_server *server,
				   t_client_infos *client)
{
  char			list[4096];
  unsigned int		i;
  t_client_infos	*dest;

  i = 0;
  memset(list, 0, 4096);
  while (i < server->list->length)
    {
      dest = (t_client_infos *)(list_find(server->list, i)->data);
      if (strcmp(dest->channel, client->channel) == 0)
	{
	  strcat(list, dest->nickname);
	  strcat(list, "\n");
	}
      ++i;
    }
  send_user_list(server, client, list);
}
