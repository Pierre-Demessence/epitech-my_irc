/*
** command.h for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Tue Apr 22 17:02:00 2014 taieb_t$
** Last update Tue Apr 22 17:03:25 2014 taieb_t$
*/

#ifndef		COMMAND_H_
# define	COMMAND_H_

# include	"server.h"

typedef struct	s_command
{
  char		*command;
  void		(*fptr)(t_server *, t_client_infos *);
}		t_command;

#endif /* !COMMAND_H_ */
