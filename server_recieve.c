/*
** server_recieve.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Tue Apr 22 16:56:44 2014 taieb_t$
** Last update Thu Apr 24 16:30:05 2014 taieb_t$
*/

#include <string.h>
#include "server.h"
#include "channels.h"
#include "command.h"

static t_command g_commands[] =
  {
    {"/nick ", server_nickname},
    {"/msg ", server_private_message},
    {"/join ", join_channel},
    {"/part ", leave_channel},
    {"/users", users_list},
    {"/quit", delete_client}
  };

void		server_get_line(t_server *server, t_client_infos *client)
{
  int		i;

  i = 0;
  client->last_read = read_next(client->c_buffer);
  while (i < 6)
    {
      if (strncmp(client->last_read, g_commands[i].command,
		  strlen(g_commands[i].command)) == 0)
	{
	  g_commands[i].fptr(server, client);
	  if (i != 5)
	    memset(client->last_read, 0, BUFFER_SIZE);
	  return ;
	}
      ++i;
    }
  basic_message_send(server, client);
  memset(client->last_read, 0, BUFFER_SIZE);
}
