/*
** channels.h for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Tue Apr 22 18:32:07 2014 taieb_t$
** Last update Thu Apr 24 15:20:00 2014 taieb_t$
*/

#ifndef		CHANNELS_H_
# define	CHANNELS_H_

# define _BSD_SOURCE
# include <string.h>
# include <stdlib.h>
# include <stdio.h>
# include "server.h"

void		leave_channel(t_server *, t_client_infos *);
void		join_channel(t_server *, t_client_infos *);
void		users_list(t_server *, t_client_infos *);

#endif /* !CHANNELS_H_ */
