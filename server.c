/*
** server.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Mon Apr 21 14:56:46 2014 taieb_t$
** Last update Sun Apr 27 11:37:44 2014 taieb_t$
*/

#include <stdio.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include "server.h"
#include "port.h"

static void		read_is_set(t_server *server)
{
  unsigned int		i;
  int			len;
  t_client_infos	*client;
  char			tempo[BUFFER_SIZE];

  i = 0;
  while (i < server->list->length)
    {
      client = (t_client_infos *)list_find(server->list, i)->data;
      if (FD_ISSET(client->cs, &server->readfs))
	{
	  memset(tempo, 0, BUFFER_SIZE);
	  if ((len = read(client->cs, tempo, BUFFER_SIZE - 1)) <= 0)
	    {
	      close(client->cs);
	      list_pop(server->list, i, 1);
	      return ;
	    }
	  write_next(client->c_buffer, tempo);
	  server_get_line(server, client);
	}
      ++i;
    }
}

static void		write_is_set(t_server *server)
{
  unsigned int		i;
  t_client_infos	*client;

  i = -1;
  while (++i < server->list->length)
    {
      client = (t_client_infos *)list_find(server->list, i)->data;
      if (FD_ISSET(client->cs, &server->writefs))
      	{
	  if ((client->last_read = read_next(client->c_buffer_w)) == NULL
	      || strlen(client->last_read) <= 0)
	    continue ;
	  write(client->cs, client->last_read, strlen(client->last_read));
	  memset(client->last_read, 0, BUFFER_SIZE);
      	}
    }
}

static void		main_loop_server(t_server *server)
{
  while (1)
    {
      set_all_fd(server);
      if (select(get_max(server), &server->readfs,
		 &server->writefs, NULL, NULL) == -1)
	break ;
      if (FD_ISSET(server->sfd, &server->readfs))
	add_client(server);
      else
	{
	  read_is_set(server);
	  write_is_set(server);
	}
    }
}

int	main(int ac, char **av)
{
  t_server		server;

  if (ac <= 1)
    server.port = DEFAULT_PORT;
  else
    server.port = atoi(av[1]);
  if (init_server(&server))
    return (1);
  sigquit((unsigned long)&server);
  (void)signal(SIGINT, (void(*)(int))sigquit);
  if ((server.list = list_new()) == NULL)
    return (1);
  main_loop_server(&server);
  close(server.sfd);
  return (0);
}
