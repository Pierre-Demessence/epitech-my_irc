/*
** server_delete_client.c for  in /home/taieb_t/Desktop/project/epitech-my_irc
**
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
**
** Started on  Thu Apr 24 15:40:05 2014 taieb_t$
** Last update Sat Apr 26 17:54:20 2014 demess_p
*/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "server.h"

static void		send_disconnect(t_server *server,
					t_client_infos *client)
{
  unsigned int		i;
  t_client_infos	*dest;

  i = 0;
  while (i < server->list->length)
    {
      dest = (t_client_infos *)(list_find(server->list, i)->data);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      if (strcmp(dest->channel, client->channel) == 0)
  	{
	  strcat(dest->buffer_tempo, client->nickname);
	  strcat(dest->buffer_tempo, " : Disconnected\n");
  	}
      write_next(dest->c_buffer, dest->buffer_tempo);
      memset(dest->buffer_tempo, 0, BUFFER_SIZE);
      ++i;
    }
}

void			delete_client(t_server *server,
				      t_client_infos *client)
{
  unsigned int		i;
  t_client_infos	*test;

  i = 0;
  while (i < server->list->length)
    {
      test = (t_client_infos *)(list_find(server->list, i)->data);
      if (client == test)
	{
	  send_disconnect(server, client);
	  list_pop(server->list, i, 0);
	  close(client->cs);
	  free(client->c_buffer);
	  free(client);
	  return ;
	}
      ++i;
    }
}
